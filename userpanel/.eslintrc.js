module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": "plugin:vue/essential",
  "parserOptions": {
    "parser": "babel-eslint",
    "ecmaVersion": 2017,
    "sourceType": "module"
  },
  "rules": {
    "indent": [
      "error",
      "tab"
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "never"
    ]
  },
  "plugins": [
    "vue",
  ]
};