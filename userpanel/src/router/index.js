import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/pages/index')
    },
    {
      path: '/users',
      name: 'user-index',
      component: () => import('@/pages/users/index')
    },
    {
      path: '/users/add',
      name: 'user-add',
      component: () => import('@/pages/users/add')
    },
    {
      path: '/users/:id',
      name: 'user-edit',
      component: () => import('@/pages/users/_id'),
      props: true
    }
  ]
})
